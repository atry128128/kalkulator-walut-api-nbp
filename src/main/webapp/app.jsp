<%@ page import="com.Mapping" %>
<%@ page import="static com.Mapping.listaWalut" %>
<%@ page import="com.wyjatki.BrakDanychError" %>
<%--
  Created by IntelliJ IDEA.
  User: Joanna Kwaśniewska
  Date: 27.11.2019
  Time: 18:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <meta charset="UTF-8">
    <title>Form</title>
    <style>
        body {
            background-color: pink;
            margin: auto;

        }

        div {
            border: 8px solid rebeccapurple;
            border-radius: 5px;
            padding: 10px;
            background-color: hotpink;
            margin: auto;
            width: 280px;
            text-align: center;
        }
    </style>
</head>
<body><br><br>
<h1>
    <legend style=" text-align:center; font-family: 'Arial Black'">Kalkulator walut</legend>
</h1>
<div>

    <form method="get" action="app.jsp">


        <label>
            Kwota:<br>
            <input name="kwota" type="number">
        </label>
        <br><br>


        Waluta podanej kwoty:<br>

        <select name="walutaIn">
            <%

                for (String currency : listaWalut) {
            %>
            <option value="<%=currency%>"><%=currency%>
            </option>
            <%
                }%>

        </select><br><br>


        Waluta, na która chcesz przeliczyć kwotę:<br>
        <select name="walutaOut">

            <%

                for (String currency : listaWalut) {
            %>
            <option value="<%=currency%>"><%=currency%>
            </option>
            <%
                }%>


        </select><br><br>


        <input type="submit" value="Przelicz"><br>
    </form>
</div>
<br>

<div style="font-size:30px; font-weight:500;width:40%;">

    <%
        String kwota = request.getParameter("kwota");


        if (kwota == null) {
            try {
                throw new BrakDanychError("Wpisz kwotę, którą chcesz przeliczyć ;-)");
            } catch (BrakDanychError brakDanychError) {
                brakDanychError.printStackTrace();
            }
        }

        String walutaIn = request.getParameter("walutaIn");
        String walutaOut = request.getParameter("walutaOut");
        Mapping maps = new Mapping();
        if (walutaIn != null || walutaOut != null) {
            Double kwotaInt = Double.parseDouble(kwota);

            double wynik = maps.przelicz(walutaIn, walutaOut, kwotaInt);
    %>
    <%=kwotaInt + walutaIn %> = <%=String.format("%.2f", wynik)%>  <%=walutaOut%>
    <%}%>
</div>

</body>
</html>
