package com;

import com.google.gson.Gson;
import com.wyjatki.BrakDanychError;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Mapping {
    public String ADRES_BAZOWY = "http://api.nbp.pl/api/";
    public static List<String> listaWalut = new ArrayList<>();

    static {
        listaWalut.add("USD");
        listaWalut.add("EUR");
        listaWalut.add("CZK");
        listaWalut.add("CHF");
    }

    public static String pobieranieJSONzURL(String urlText) {
        try {
            URL myUrl = new URL(urlText);
            StringBuilder jsonText = new StringBuilder();
            try (InputStream myInputStream = myUrl.openStream();
                 Scanner scanner = new Scanner(myInputStream)) {
                while (scanner.hasNextLine()) {
                    jsonText.append(scanner.nextLine());
                }
                return jsonText.toString();
            }
        } catch (IOException e) {
            return null;
        }

    }

    public double przelicz(String walutaIn, String walutaOut, Double kwota) throws BrakDanychError {

        if (kwota == null) {
            throw new BrakDanychError("Wpisz kwotę, którą chcesz przeliczyć ;-)");
        }

        String nbpJsonWalutaIn = pobieranieJSONzURL(String.format(ADRES_BAZOWY + "exchangerates/rates/a/%s/?format=json", walutaIn));
        String nbpJsonWalutaOut = pobieranieJSONzURL(String.format(ADRES_BAZOWY + "exchangerates/rates/a/%s/?format=json", walutaOut));
        Gson gson = new Gson();
        Currency currencyWalutaIn = gson.fromJson(nbpJsonWalutaIn, Currency.class);
        Currency currencyWalutaOut = gson.fromJson(nbpJsonWalutaOut, Currency.class);

        if (currencyWalutaIn != null && currencyWalutaOut != null) {
            Double walutaInRateValue = currencyWalutaIn.getRates().get(0).getMid();
            Double walutaOutRateValue = currencyWalutaOut.getRates().get(0).getMid();
            return (kwota * walutaInRateValue) / walutaOutRateValue;
        }
        return 0;
    }
}
