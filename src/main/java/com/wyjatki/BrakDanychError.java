package com.wyjatki;

public class BrakDanychError extends Exception {
    public BrakDanychError (String message){
        super(message);
    }
}
