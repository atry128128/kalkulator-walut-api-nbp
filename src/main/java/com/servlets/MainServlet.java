package com.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet({"/", "/index", "/index.html"})
public class MainServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("text/html;charset=UTF-8");

        resp.getWriter().write("Aplikacja do przeliczania walut korzystająca z API Narodowego Banku Polskiego<br>");
        resp.getWriter().write("<a href=\"app.jsp\">Wejdź!</a>");
    }
}
